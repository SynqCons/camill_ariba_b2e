DEF TEMP-TABLE tt-integra NO-UNDO
    FIELD CNPJ                   AS c
    FIELD RazaoSocial            AS c
    FIELD NaturezaJuridica       AS c
    FIELD CEP                    AS c
    FIELD LOGRADOURO             AS c
    FIELD COMPLEMENTO            AS c
    FIELD CIDADE                 AS c
    FIELD BAIRRO                 AS c
    FIELD UF                     AS c
    FIELD SintegraAtivo          AS c
    FIELD CNPJAtivo              AS c
    FIELD OptanteSimplesNacional AS c
    FIELD CPF                    AS c
    FIELD NOME                   AS c
    FIELD CPFAtivo               AS c
    FIELD InscricaoEstadual      AS c
    FIELD CNAE                   AS c
    FIELD Mensagem               AS c
    FIELD Parecer                AS c
    FIELD Motivo                 AS c
    FIELD CodigoPropostaCliente  AS c.


